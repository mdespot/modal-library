import React from "react";
import { Modal } from "../library/Modal";

export const MyModal = () => (
  <Modal trigger={<button>Click to open modal</button>}>
    <div>
      <h3>Product details</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque purus
        turpis, posuere eu consectetur ac, scelerisque et mi. Quisque non
        suscipit lorem, vel vulputate nibh. Interdum et malesuada fames ac ante
        ipsum primis in faucibus. Sed condimentum massa eros. Aliquam at varius
        diam. Phasellus posuere nulla sed nibh venenatis imperdiet. Quisque
        vitae purus ornare, interdum nisl vel, consectetur libero. Donec viverra
        a tellus at gravida.
      </p>
    </div>
    <div>
      <h3>Security policy</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque purus
        turpis, posuere eu consectetur ac, scelerisque et mi. Quisque non
        suscipit lorem, vel vulputate nibh. Interdum et malesuada fames ac ante
        ipsum primis in faucibus. Sed condimentum massa eros. Aliquam at varius
        diam. Phasellus posuere nulla sed nibh venenatis imperdiet. Quisque
        vitae purus ornare, interdum nisl vel, consectetur libero. Donec viverra
        a tellus at gravida.
      </p>
    </div>
  </Modal>
);
