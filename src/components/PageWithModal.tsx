import * as React from "react";
import { MyModal } from "./MyModal";

export const PageWithModal = () => {
  return (
    <div className="page">
      <h1>React model library</h1>
      <div className="info">
        <p className="description">
          For more details about our product and to preview our security policy,
          click on the button bellow.
        </p>
        <MyModal />
      </div>
    </div>
  );
};
