import React, { useState, ReactElement } from "react";
import ReactDOM from "react-dom";
import close from "../assets/img/close.svg";

interface MyModal {
  children: Array<ReactElement>;
  trigger: ReactElement;
}

export const Modal = (props: MyModal) => {
  const [showModal, setShowModal] = useState<boolean>(false);

  const modalRoot: any = document.getElementById("modal");
  const rootId: any = document.getElementById("root");

  const handleClick = () => {
    setShowModal(showModal => !showModal);

    showModal
      ? (rootId.style.filter = "none")
      : (rootId.style.filter = "blur(3px)");
  };

  return (
    <div>
      <div onClick={handleClick}>{props.trigger}</div>

      {showModal &&
        ReactDOM.createPortal(
          <div className="modal">
            <div className="closeButton">
              <img src={close} className="imageModal" onClick={handleClick} />
            </div>
            {props.children}
          </div>,
          modalRoot
        )}
    </div>
  );
};
