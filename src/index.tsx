import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {PageWithModal} from './components/PageWithModal';
import './index.css';

ReactDOM.render(<PageWithModal />, document.getElementById('root'));


